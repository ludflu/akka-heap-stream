# akka-heap-stream

If you want to buffer items in akka streams, but you would prefer that they are emitted in priority order,
you can use this custom graph stage to reorder the elements as they accumulate.  For example:

```scala
    val stream =
      Source(scala.util.Random.shuffle(1 to 10).toList)
        .via(new PriorityBuffer[Int](100))
        .delay(1.second)
        .runWith(Sink.seq)
    val results = Await.result(stream,10.seconds)
    println(results)
    
    > Vector(10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
```

or, if you prefer the convenience of an implicit:

```scala
    import HeapStream.PrioritizedSource.implicits._

    val stream =
      Source(scala.util.Random.shuffle(1 to 10).toList)
        .prioritize(maxSize=100)
        .runWith(Sink.seq)
    val results = Await.result(stream,10.seconds)
    println(results)
    
    > Vector(10, 9, 8, 7, 6, 5, 4, 3, 2, 1)
```

