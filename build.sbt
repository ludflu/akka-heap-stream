name := "akka-stream-heap"

version := "0.1"

scalaVersion := "2.12.6"
val AkkaVersion = "2.5.13"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
  "org.scalatest"     %% "scalatest" % "3.0.5" % "test"
)