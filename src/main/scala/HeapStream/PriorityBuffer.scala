package HeapStream

import akka.stream.stage._
import akka.stream.{Attributes, FlowShape, Inlet, Outlet}

class PriorityBuffer[T] (val maxSize: Int)(implicit o:Ordering[T]) extends GraphStage[FlowShape[T, T]]{
  val in = Inlet[T]("priority.in")
  val out = Outlet[T]("priority.out")
  override val shape = FlowShape.of(in, out)

  override def createLogic(inheritedAttributes: Attributes): GraphStageLogic = new GraphStageLogic(shape) {
    private val pbuffer = collection.mutable.PriorityQueue[T]()

    setHandler(out, new OutHandler {
      override def onPull(): Unit = {
        if (!isClosed(in) && !hasBeenPulled(in)) {
          pull(in)
        }
        emit()
      }
    })

    setHandler(in, new InHandler {
      override def onPush(): Unit = {
        val elem = grab(in)

        if (pbuffer.size < maxSize) {
          pbuffer.enqueue(elem)
          if (!isClosed(in)) {
            pull(in)
          }
        } else {
          if (isAvailable(out)) {
            push(out, elem)
          }
        }
      }

      override def onUpstreamFinish(): Unit = {
        if (pbuffer.isEmpty) {
          completeStage()
        } else {
            emit()
        }
      }
    })

    private def emit(): Unit = {
      if (pbuffer.isEmpty) {
        if (isClosed(in)) {
          completeStage()
        }
      } else {
          if (isAvailable(out)) {
            push(out, pbuffer.dequeue())
          }
      }
    }
  }
}