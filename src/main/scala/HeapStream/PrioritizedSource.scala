package HeapStream

import akka.NotUsed
import akka.stream.scaladsl.Source

object PrioritizedSource {
  object implicits {
    implicit def toPrioritizedSource[T](s:Source[T,NotUsed]) = new PrioritizedSource(s)
  }
}

class PrioritizedSource[T](s:Source[T,NotUsed]) {
  def prioritize(maxSize:Int)(implicit o:Ordering[T]) : Source[T,NotUsed] =
    s.via(new PriorityBuffer[T](maxSize))
}
