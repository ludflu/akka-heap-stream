package HeapStream

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}
import org.scalatest.FlatSpec

import scala.collection.immutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._


class PriorityBufferSpec extends FlatSpec {

  import HeapStream.PrioritizedSource.implicits._

  implicit val system = ActorSystem("test-system")
  implicit val materializer = ActorMaterializer()
  implicit val executionContext = system.dispatcher

  case class TestJob(name:String, customer:String, priority:Float)

  object JobOrdering extends Ordering[TestJob] {
    override def compare(x: TestJob, y: TestJob): Int = x.priority compare y.priority
  }

  val jobs = (1 to 10).map(i => TestJob(s"job$i", "customer", i.toFloat)).reverse
  scala.util.Random.setSeed(1234L)
  val scrambled = scala.util.Random.shuffle(jobs).toList

  def runjobs(jobs:List[TestJob], size:Int): immutable.Seq[TestJob] = {

    val slowConsumer: Sink[TestJob, Future[immutable.Seq[TestJob]]] =
      Flow[TestJob]
      .delay(1.second)
      .toMat(Sink.seq)(Keep.right)

    val f = Source(jobs)
      .prioritize(size)(JobOrdering)
      .async
      .runWith(slowConsumer)

    Await.result(f, 10.seconds)
  }

  "buffered items" should "be accumulated in priority order" in {
    val results = runjobs(scrambled, 10)
    assert( results == jobs )
  }

  "small priority buffer" should "accumulatr in priority order up to the buffer size" in {
    val results = runjobs(scrambled,5)
    assert( results.map(_.priority) == Vector(8.0, 10.0, 9.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0, 1.0))
  }

  "items with no back pressure" should "get reordered too" in {
    val nopressure =
      Source(scrambled)
        .prioritize(100)(JobOrdering)
        .runWith(Sink.seq)
    val results = Await.result(nopressure,10.seconds)
    assert(results == jobs)
  }

}
